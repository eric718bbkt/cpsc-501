package A1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Eric
 */
public class FormulaData {
	/** The formula representing the compound. */
	private String formula;
	/** The mass of the compound. */
	private double mass;
	/** An array of invalid elements, as strings, present in the compound. */
	private List<String> invalidElements = new ArrayList<String>();
	/** The integer count of each element in the compound. */
	private HashMap<String, Integer> elementCounts = new HashMap<String, Integer>();
	/** The total mass of each element in the compound. */
	private HashMap<String, Double> elementMasses = new HashMap<String, Double>();
	
	/**
	 * Constructs a new instance of the {@link FormulaData} class.
	 * 
	 * @param compoundFormula
	 */
	public FormulaData(String compoundFormula) {
		formula = compoundFormula;
	}
	
	/**
	 * Adds a single element to the compound and sets its data. Will increment data for this element if it's already added.
	 * 
	 * @param element
	 *            The {@link Element} representing the component of the compound.
	 * @param count
	 *            The number of the element.
	 */
	public void addElement(Element element, int count) {
		String elementName = element.getShortName();
		
		if (elementCounts.containsKey(elementName)) {
			count += elementCounts.get(elementName);
		}
		
		double elementMass = count * element.getWeight().doubleValue();
		
		elementCounts.put(elementName, count);
		elementMasses.put(elementName, elementMass);
	}
	
	/**
	 * @return The formula representing the compound.
	 */
	public String getFormula() {
		return formula;
	}
	
	/**
	 * @return The mass of the compound.
	 */
	public double getMass() {
		return mass;
	}
	
	/**
	 * @param mass
	 *            The new mass of the compound. Should be calculated or set only by {@link FindChemWeight#getMolecularWeight(String, HashMap, HashMap, List)}.
	 */
	public void setMass(double mass) {
		this.mass = mass;
	}
	
	/**
	 * @return An array of invalid elements, as strings, present in the compound.
	 */
	public List<String> getInvalidElements() {
		return invalidElements;
	}
	
	/**
	 * @return The integer count of each element in the compound.
	 */
	public HashMap<String, Integer> getElementCounts() {
		return elementCounts;
	}
	
	/**
	 * @return The percentage mass of each element in the compound. This is based on the current value of {@link #mass}.
	 */
	public HashMap<String, Double> getElementPercentages() {
		HashMap<String, Double> percents = new HashMap<String, Double>();
		
		for (Map.Entry<String, Double> entry : elementMasses.entrySet()) {
			percents.put(entry.getKey(), entry.getValue() / mass * 100.0);
		}
		
		return percents;
	}
}
