package A1;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Represents a chemical element.
 * 
 * @author Eric Kumlin
 */
public class Element {
	/** Keeps a full list of all chemical elements. */
	private static ArrayList<Element> elementsList = new ArrayList<Element>();
	
	/** The weight of the chemical element in <code>g/mol</code>. */
	private BigDecimal weight;
	
	/** The abbreviation of the chemical element, such as <code>He</code> for helium. */
	private String shortName;
	
	/**
	 * @return A full list of all chemical elements.
	 */
	public static ArrayList<Element> getElementList() {
		return elementsList;
	}
	
	/**
	 * Initializes a full element list of all known elements.
	 */
	public static void initializeElementList() {
		elementsList.clear();
		
		elementsList.add(new Element("H", "1.0079"));
		elementsList.add(new Element("He", "4.0026"));
		elementsList.add(new Element("Li", "6.941"));
		elementsList.add(new Element("Be", "9.0122"));
		elementsList.add(new Element("B", "10.811"));
		elementsList.add(new Element("C", "12.0107"));
		elementsList.add(new Element("N", "14.0067"));
		elementsList.add(new Element("O", "15.9994"));
		elementsList.add(new Element("F", "18.9984"));
		elementsList.add(new Element("Ne", "20.1797"));
		elementsList.add(new Element("Na", "22.9897"));
		elementsList.add(new Element("Mg", "24.305"));
		elementsList.add(new Element("Al", "26.9815"));
		elementsList.add(new Element("Si", "28.0855"));
		elementsList.add(new Element("P", "30.9738"));
		elementsList.add(new Element("S", "32.065"));
		elementsList.add(new Element("Cl", "35.453"));
		elementsList.add(new Element("Ar", "39.948"));
		elementsList.add(new Element("K", "39.0983"));
		elementsList.add(new Element("Ca", "40.078"));
		elementsList.add(new Element("Sc", "44.9559"));
		elementsList.add(new Element("Ti", "47.867"));
		elementsList.add(new Element("V", "50.9415"));
		elementsList.add(new Element("Cr", "51.9961"));
		elementsList.add(new Element("Mn", "54.938"));
		elementsList.add(new Element("Fe", "55.845"));
		elementsList.add(new Element("Co", "58.9332"));
		elementsList.add(new Element("Ni", "58.6934"));
		elementsList.add(new Element("Cu", "63.546"));
		elementsList.add(new Element("Zn", "65.39"));
		elementsList.add(new Element("Ga", "69.723"));
		elementsList.add(new Element("Ge", "72.64"));
		elementsList.add(new Element("As", "74.9216"));
		elementsList.add(new Element("Se", "78.96"));
		elementsList.add(new Element("Br", "79.904"));
		elementsList.add(new Element("Kr", "83.8"));
		elementsList.add(new Element("Rb", "85.4678"));
		elementsList.add(new Element("Sr", "87.62"));
		elementsList.add(new Element("Y", "88.9059"));
		elementsList.add(new Element("Zr", "91.224"));
		elementsList.add(new Element("Nb", "92.9064"));
		elementsList.add(new Element("Mo", "95.94"));
		elementsList.add(new Element("Tc", "98"));
		elementsList.add(new Element("Ru", "101.07"));
		elementsList.add(new Element("Rh", "102.9055"));
		elementsList.add(new Element("Pd", "106.42"));
		elementsList.add(new Element("Ag", "107.8682"));
		elementsList.add(new Element("Cd", "112.411"));
		elementsList.add(new Element("In", "114.818"));
		elementsList.add(new Element("Sn", "118.71"));
		elementsList.add(new Element("Sb", "121.76"));
		elementsList.add(new Element("Te", "127.6"));
		elementsList.add(new Element("I", "126.9045"));
		elementsList.add(new Element("Xe", "131.293"));
		elementsList.add(new Element("Cs", "132.9055"));
		elementsList.add(new Element("Ba", "137.327"));
		elementsList.add(new Element("La", "138.9055"));
		elementsList.add(new Element("Ce", "140.116"));
		elementsList.add(new Element("Pr", "140.9077"));
		elementsList.add(new Element("Nd", "144.24"));
		elementsList.add(new Element("Pm", "145"));
		elementsList.add(new Element("Sm", "150.36"));
		elementsList.add(new Element("Eu", "151.964"));
		elementsList.add(new Element("Gd", "157.25"));
		elementsList.add(new Element("Tb", "158.9253"));
		elementsList.add(new Element("Dy", "162.5"));
		elementsList.add(new Element("Ho", "164.9303"));
		elementsList.add(new Element("Er", "167.259"));
		elementsList.add(new Element("Tm", "168.9342"));
		elementsList.add(new Element("Yb", "173.04"));
		elementsList.add(new Element("Lu", "174.967"));
		elementsList.add(new Element("Hf", "178.49"));
		elementsList.add(new Element("Ta", "180.9479"));
		elementsList.add(new Element("W", "183.84"));
		elementsList.add(new Element("Re", "186.207"));
		elementsList.add(new Element("Os", "190.23"));
		elementsList.add(new Element("Ir", "192.217"));
		elementsList.add(new Element("Pt", "195.078"));
		elementsList.add(new Element("Au", "196.9665"));
		elementsList.add(new Element("Hg", "200.59"));
		elementsList.add(new Element("Tl", "204.3833"));
		elementsList.add(new Element("Pb", "207.2"));
		elementsList.add(new Element("Bi", "208.9804"));
		elementsList.add(new Element("Po", "209"));
		elementsList.add(new Element("At", "210"));
		elementsList.add(new Element("Rn", "222"));
		elementsList.add(new Element("Fr", "223"));
		elementsList.add(new Element("Ra", "226"));
		elementsList.add(new Element("Ac", "227"));
		elementsList.add(new Element("Th", "232.0381"));
		elementsList.add(new Element("Pa", "231.0359"));
		elementsList.add(new Element("U", "238.0289"));
		elementsList.add(new Element("Np", "237"));
		elementsList.add(new Element("Pu", "244"));
		elementsList.add(new Element("Am", "243"));
		elementsList.add(new Element("Cm", "247"));
		elementsList.add(new Element("Bk", "247"));
		elementsList.add(new Element("Cf", "251"));
		elementsList.add(new Element("Es", "252"));
		elementsList.add(new Element("Fm", "257"));
		elementsList.add(new Element("Md", "258"));
		elementsList.add(new Element("No", "259"));
		elementsList.add(new Element("Lr", "262"));
		elementsList.add(new Element("Rf", "261"));
		elementsList.add(new Element("Db", "262"));
		elementsList.add(new Element("Sg", "266"));
		elementsList.add(new Element("Bh", "264"));
		elementsList.add(new Element("Hs", "277"));
		elementsList.add(new Element("Mt", "268"));
		elementsList.add(new Element("Ds", "271"));
		elementsList.add(new Element("Rg", "272"));
		elementsList.add(new Element("Cn", "285"));
		elementsList.add(new Element("Uut", "284"));
		elementsList.add(new Element("Fl", "289"));
		elementsList.add(new Element("Uup", "288"));
		elementsList.add(new Element("Lv", "290"));
		elementsList.add(new Element("Uus", "291"));
		elementsList.add(new Element("Uuo", "294"));
	}
	
	/**
	 * Constructs a new instance of the {@link Element} class.
	 * 
	 * @param elementShortName
	 *            The abbreviation of the chemical element, such as <code>He</code> for helium.
	 * @param elementWeight
	 *            The weight of the chemical element in <code>g/mol</code>.
	 */
	public Element(String elementShortName, String elementWeight) {
		shortName = elementShortName;
		weight = new BigDecimal(elementWeight);
	}
	
	/**
	 * @return The weight of the chemical element in <code>g/mol</code>. This is immutable per definition of {@link BigDecimal}.
	 */
	public BigDecimal getWeight() {
		return weight;
	}
	
	/**
	 * @return The abbreviation of the chemical element, such as <code>He</code> for helium.
	 */
	public String getShortName() {
		return shortName;
	}
}
