package A1.Tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import A1.Element;

/**
 * Runs automated tests for the {@link Element} class.
 * 
 * @author Eric
 */
public class ElementTest {
	/**
	 * Test the ability to create an element and have it retain data correctly.
	 */
	@Test
	public void testElementConstruction() {
		final String nameValue = "Na";
		final String weightValue = "22.99";
		
		Element testElement = new Element(nameValue, weightValue);
		
		assertEquals(nameValue, testElement.getShortName());
		assertEquals(weightValue, testElement.getWeight().toString());
	}
}
