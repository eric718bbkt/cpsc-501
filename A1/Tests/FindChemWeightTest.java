package A1.Tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import A1.Element;
import A1.FindChemWeight;
import A1.FormulaData;

/**
 * Runs automated tests for the {@link FindChemWeight} class.
 * 
 * @author Eric
 */
public class FindChemWeightTest {
	/**
	 * Initialize the element list.
	 * 
	 * @throws Exception
	 *             Thrown if inner methods throw exceptions.
	 */
	@Before
	public void setUp() throws Exception {
		Element.initializeElementList();
	}
	
	/**
	 * Test that the {@link FindChemWeight#getMolecularWeight(String, HashMap, HashMap, java.util.List)} method returns accurate mass, groups, percentages, and errors.
	 */
	@Test
	public void testRobustFormula() {
		String formula = "C2H(HO2)2";
		
		// Get actual results
		FormulaData data = FindChemWeight.getMolecularWeight(formula);
		double mass = data.getMass();
		HashMap<String, Integer> groups = data.getElementCounts();
		HashMap<String, Double> percents = data.getElementPercentages();
		List<String> errors = data.getInvalidElements();
		
		// Define expected results
		double carbonMassExpected = 12.011 * 2;
		double hydrogenMassExpected = 1.008 * 3;
		double oxygenMassExpected = 15.999 * 4;
		double impreciseMassExpected = carbonMassExpected + hydrogenMassExpected + oxygenMassExpected;
		double massExpected = Math.round(impreciseMassExpected * 1000.0) / 1000.0; // Round to 3 digits
		
		HashMap<String, Integer> groupsExpected = new HashMap<String, Integer>();
		groupsExpected.put("C", 2);
		groupsExpected.put("H", 3);
		groupsExpected.put("O", 4);
		
		HashMap<String, Double> percentsExpected = new HashMap<String, Double>();
		percentsExpected.put("C", carbonMassExpected / impreciseMassExpected * 100.0);
		percentsExpected.put("H", hydrogenMassExpected / impreciseMassExpected * 100.0);
		percentsExpected.put("O", oxygenMassExpected / impreciseMassExpected * 100.0);
		
		ArrayList<String> errorsExpected = new ArrayList<String>();
		
		// Assert basic equalities
		assertEquals(groupsExpected, groups);
		assertEquals(errorsExpected, errors);
		assertEquals(massExpected, mass, 0.001);
		
		// Assert each percentage separately since some might be slightly different due to lower precision in test case (1% error).
		for (Map.Entry<String, Double> entry : percentsExpected.entrySet()) {
			assertEquals(percentsExpected.get(entry.getKey()), percents.get(entry.getKey()), 0.01);
		}
	}
	
	/**
	 * Test that the {@link FindChemWeight#getMolecularWeight(String, HashMap, HashMap, java.util.List)} method returns empty data for an empty formula.
	 */
	@Test
	public void testEmptyFormula() {
		String formula = "";
		
		// Get actual results
		FormulaData data = FindChemWeight.getMolecularWeight(formula);
		double mass = data.getMass();
		HashMap<String, Integer> groups = data.getElementCounts();
		HashMap<String, Double> percents = data.getElementPercentages();
		List<String> errors = data.getInvalidElements();
		
		// Define expected results
		HashMap<String, Integer> groupsExpected = new HashMap<String, Integer>();
		HashMap<String, Double> percentsExpected = new HashMap<String, Double>();
		ArrayList<String> errorsExpected = new ArrayList<String>();
		
		double massExpected = 0.0;
		
		// Assert basic equalities
		assertEquals(groupsExpected, groups);
		assertEquals(errorsExpected, errors);
		assertEquals(massExpected, mass, 0.0);
		
		// Assert each percentage separately.
		for (Map.Entry<String, Double> entry : percentsExpected.entrySet()) {
			assertEquals(percentsExpected.get(entry.getKey()), percents.get(entry.getKey()), 0.0);
		}
	}
	
	/**
	 * Test that the {@link FindChemWeight#getMolecularWeight(String, HashMap, HashMap, java.util.List)} method returns an error and no data for an invalid element.
	 */
	@Test
	public void testInvalidFormula() {
		String formula = "X";
		
		// Get actual results
		FormulaData data = FindChemWeight.getMolecularWeight(formula);
		double mass = data.getMass();
		HashMap<String, Integer> groups = data.getElementCounts();
		HashMap<String, Double> percents = data.getElementPercentages();
		List<String> errors = data.getInvalidElements();
		
		// Define expected results
		HashMap<String, Integer> groupsExpected = new HashMap<String, Integer>();
		HashMap<String, Double> percentsExpected = new HashMap<String, Double>();
		ArrayList<String> errorsExpected = new ArrayList<String>();
		errorsExpected.add("X");
		
		double massExpected = 0.0;
		
		// Assert basic equalities
		assertEquals(groupsExpected, groups);
		assertEquals(errorsExpected, errors);
		assertEquals(massExpected, mass, 0.0);
		
		// Assert each percentage separately.
		for (Map.Entry<String, Double> entry : percentsExpected.entrySet()) {
			assertEquals(percentsExpected.get(entry.getKey()), percents.get(entry.getKey()), 0.0);
		}
	}
}
