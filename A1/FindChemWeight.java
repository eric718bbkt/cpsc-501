package A1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The main class for the chemical weight finder application.
 * 
 * @author Eric
 */
public class FindChemWeight {
	/**
	 * Main method of program. Requests molecule input from the user, and prints out mass and related data. Runs only once.
	 * 
	 * @param args
	 *            Arguments to be passed to the program. They will be discarded.
	 */
	public static void main(String[] args) {
		Element.initializeElementList();
		
		String formula = readLineOrDefault("Enter molecule", "H2Cl3");
		
		StringBuilder formulaBuilder = new StringBuilder();
		for (int i = 0; i < formula.length(); i++) {
			char c = formula.charAt(i);
			if (i == 0 || (Character.isDigit(formula.charAt(i - 1)) && Character.isLetter(c))) {
				formulaBuilder.append(Character.toUpperCase(c));
			} else {
				formulaBuilder.append(c);
			}
		}
		formula = formulaBuilder.toString();
		
		FormulaData data = getMolecularWeight(formula);
		
		double mass = data.getMass();
		HashMap<String, Integer> groups = data.getElementCounts();
		HashMap<String, Double> percents = data.getElementPercentages();
		List<String> errors = data.getInvalidElements();

		System.out.printf("* Mass: %.3f\n", mass);
		System.out.printf("* Element groups: %s\n", groups);
		System.out.printf("* Element percentages: %s\n", percents);
		System.out.printf("* Invalid elements: %s\n", errors);
	}
	
	/**
	 * Gets the molecular weight of a chemical compound as a <code>double</code>.
	 * 
	 * @param formula
	 *            The chemical formula as a string, e.g. "C2H(OH)3".
	 * @return The data representing the compound as a {@link FormulaData} object.
	 */
	public static FormulaData getMolecularWeight(String formula) {
		FormulaData data = new FormulaData(formula);
		
		Pattern p;
		Matcher result;
		double mass = 0;
		
		p = Pattern.compile("\\(([^\\(\\)]+)\\)([0-9]*)");
		result = p.matcher(formula);
		while (result.find()) {
			result.reset();
			StringBuffer sb = new StringBuffer();
			while (result.find()) {
				String multi = result.group(2);
				if (multi.equals("")) {
					multi = "1";
				}
				
				result.appendReplacement(sb, new String(new char[Integer.parseInt(multi)]).replace("\0", result.group(1)));
			}
			result.appendTail(sb);
			
			formula = sb.toString();
			result = p.matcher(formula);
		}
		
		p = Pattern.compile("([A-Z][a-z]*)([0-9]*)");
		result = p.matcher(formula);
		while (result.find()) {
			try {
				String elem = result.group(1);
				String multi = result.group(2);
				if (multi.equals("")) {
					multi = "1";
				}
				
				Element wanted = null;
				for (Element findMe : Element.getElementList()) {
					if (findMe.getShortName().equalsIgnoreCase(elem)) {
						wanted = findMe;
						break;
					}
				}
				
				double thisMass = wanted.getWeight().doubleValue() * Double.parseDouble(multi);
				mass += thisMass;
				
				data.addElement(wanted, Integer.parseInt(multi));
			} catch (Exception e) {
				// They provided something that wasn't an element.
				data.getInvalidElements().add(result.group(0));
			}
		}
		
		data.setMass(mass);
		
		return data;
	}
	
	/**
	 * Reads a line from the console, or returns a default if there is an error.
	 * 
	 * @param prompt
	 *            A string to prompt the user with.
	 * @param defaultValue
	 *            A string which represents the default value to return.
	 * @return The inputted string, or <code>defaultValue</code> if there was an error.
	 */
	public static String readLineOrDefault(String prompt, String defaultValue) {
		System.out.printf("%s: ", prompt);
		
		String result = defaultValue; // Default value in case console doesn't work
		try {
			BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
			result = bufferRead.readLine();
		} catch (IOException e) {
			// Since we're returning a default, do nothing if there's an exception.
		}
		
		return result;
	}
}
