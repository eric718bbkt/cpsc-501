/**
 * 
 */
package A2;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

/**
 * @author Eric
 * 
 */
public class ObjectInspectorTest {
	@Test
	public void testInspectObject() {
		ObjectInspector inspector = new ObjectInspector();
		
		Object input = new Object();
		String expected = "Declaring class: Object\n" +
			"Immediate superclass: null\n" +
			"Interfaces list: \n" +
			"Methods:\n" +
			"	protected void Object.finalize() throws java.lang.Throwable\n" +
			"	public final void Object.wait() throws java.lang.InterruptedException\n" +
			"	public final void Object.wait(long, int) throws java.lang.InterruptedException\n" +
			"	public final native void Object.wait(long) throws java.lang.InterruptedException\n" +
			"	public boolean Object.equals(java.lang.Object) throws no exceptions\n" +
			"	public String Object.toString() throws no exceptions\n" +
			"	public native int Object.hashCode() throws no exceptions\n" +
			"	public final native Class Object.getClass() throws no exceptions\n" +
			"	protected native Object Object.clone() throws java.lang.CloneNotSupportedException\n" +
			"	public final native void Object.notify() throws no exceptions\n" +
			"	public final native void Object.notifyAll() throws no exceptions\n" +
			"	private static native void Object.registerNatives() throws no exceptions\n" +
			"Constructors:\n" +
			"	public Object()\n" +
			"Fields:";
		
		// Let's redirect console output to a string:
		// Based on licensed code from http://stackoverflow.com/a/8708357/1438733 
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    PrintStream ps = new PrintStream(baos);
	    PrintStream old = System.out;
	    System.setOut(ps);
	    inspector.inspect(input, true);
	    System.out.flush();
	    System.setOut(old);
	    
		assertEquals(baos.toString().replaceAll("(\r|\n|\r\n)+", "\n").trim(), expected.trim());
	}
	
	@Test
	public void testClassAsString() {
		ObjectInspector inspector = new ObjectInspector();
		
		Class[][] input = {
			{ Byte.class, Object.class },
			{ Character.class },
			{}
		};
		String[] expected = {
			"java.lang.Byte, java.lang.Object",
			"java.lang.Character",
			""
		};
		
		for (int i = 0; i < input.length; i++) {
			assertEquals(inspector.classAsString(input[i]), expected[i]);
		}
	}
	
	@Test
	public void testRepeatString() {
		ObjectInspector inspector = new ObjectInspector();
		
		String[] input = { "a", "bb", "", "cccc", "!" };
		int[] times = { -1, 0, 1, 1, 5 };
		String[] expected = { "", "", "", "cccc", "!!!!!" };
		
		for (int i = 0; i < input.length; i++) {
			assertEquals(inspector.repeatString(input[i], times[i]), expected[i]);
		}
	}
}
