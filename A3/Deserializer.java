package A3;

/*========================================================================
 File: Deserializer.java
 Author: Eric Kumlin
 Date: November 2014
 ========================================================================*/

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;

public class Deserializer {
	Map<String, Object> createdObjects = new HashMap<String, Object>();
	List<ReferencedField> referenceFields = new ArrayList<ReferencedField>();

	private static class ReferencedField {
		Object obj;
		Field field;
		String referenceId;
		int index;

		public ReferencedField(Object o, Field f, String ref) {
			this(o, f, ref, -1);
		}

		public ReferencedField(Object o, Field f, String ref, int i) {
			obj = o;
			field = f;
			referenceId = ref;
			index = i;
		}

		public void apply(Object o) {
			try {
				if (index >= 0) {
					Array.set(field == null ? obj : field.get(obj), index, o);
				} else {
					field.set(obj, o);
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	private static Class<?> getPrimitiveClass(String shortName) {
		if ("int".equals(shortName)) {
			return Integer.class;
		}
		if ("long".equals(shortName)) {
			return Long.class;
		}
		if ("double".equals(shortName)) {
			return Double.class;
		}
		if ("float".equals(shortName)) {
			return Float.class;
		}
		if ("boolean".equals(shortName)) {
			return Boolean.class;
		}
		if ("char".equals(shortName)) {
			return Character.class;
		}
		if ("byte".equals(shortName)) {
			return Byte.class;
		}
		if ("void".equals(shortName)) {
			return Void.class;
		}
		if ("short".equals(shortName)) {
			return Short.class;
		}

		return Object.class;
	}

	public Object deserialize(Document doc) {
		createdObjects.clear();
		referenceFields.clear();

		String rootObjectId = null;

		Element root = doc.getRootElement();
		for (Object elemAsObj : root.getChildren()) {
			Element elem = (Element) elemAsObj;

			String className = elem.getAttributeValue("class");
			String id = elem.getAttributeValue("id");

			Object instantiated = null;
			Class<?> objClass;
			try {
				objClass = Class.forName(className);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				continue;
			}

			if (objClass.isArray()) {
				instantiated = instantiateArray(elem,
						objClass.getComponentType());
			} else {
				instantiated = instantiateObject(elem, objClass);
			}

			createdObjects.put(id, instantiated);

			if (rootObjectId == null
					&& "true".equals(elem.getAttributeValue("primary"))) {
				rootObjectId = id;
			}
		}

		for (ReferencedField ref : referenceFields) {
			ref.apply(createdObjects.get(ref.referenceId));
		}

		return createdObjects.get(rootObjectId);
	}

	private Object instantiateArray(Element elem, Class<?> componentType) {
		Object instantiated = Array.newInstance(componentType, elem
				.getChildren().size());

		int i = 0;
		for (Object valElemAsObj : elem.getChildren()) {
			Element valElem = (Element) valElemAsObj;

			if ("reference".equals(valElem.getName())) {
				referenceFields.add(new ReferencedField(instantiated, null,
						valElem.getTextTrim(), i));
			} else {
				Array.set(instantiated, i,
						instantiateValue(componentType, valElem.getTextTrim()));
			}

			i++;
		}

		return instantiated;
	}

	private Object instantiateObject(Element elem, Class<?> objClass) {
		Constructor<?> ctor;
		try {
			ctor = objClass.getConstructor();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			return null;
		} catch (SecurityException e) {
			e.printStackTrace();
			return null;
		}

		Object instantiated;
		try {
			instantiated = ctor.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return null;
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			return null;
		}

		for (Object fieldAsObj : elem.getChildren()) {
			Element field = (Element) fieldAsObj;
			String fieldName = field.getAttributeValue("name");

			Field f;
			try {
				f = objClass.getDeclaredField(fieldName);
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
				continue;
			} catch (SecurityException e) {
				e.printStackTrace();
				continue;
			}

			if (Modifier.isFinal(f.getModifiers())) {
				continue;
			}

			f.setAccessible(true);

			Element child = null;
			for (Object fieldValueAsObject : field.getChildren()) {
				child = (Element) fieldValueAsObject;
			}

			try {
				if ("reference".equals(child.getName())) {
					referenceFields.add(new ReferencedField(instantiated, f,
							child.getTextTrim()));
				} else if ("value".equals(child.getName())) {
					f.set(instantiated,
							instantiateValue(f.getType(), child.getTextTrim()));
				} else {
					f.set(instantiated, null);
				}
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return instantiated;
	}

	private Object instantiateValue(Class<?> type, String value) {
		Class<?> primitiveClass = getPrimitiveClass(type.getSimpleName());
		Object result = null;

		if (primitiveClass == Character.class) {
			result = value.charAt(0);
		} else if (primitiveClass == Object.class && type == Object.class) {
			result = value;
		} else {
			try {
				Constructor<?> ctor = (primitiveClass == Object.class ? type
						: primitiveClass).getConstructor(String.class);
				result = ctor.newInstance(value);
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}

		return result;
	}
}
