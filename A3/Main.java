package A3;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import A3.Tests.TestableComplexObject;

public class Main {
	public static void main(String[] args) throws IOException {
		String reply = "";

		ObjectTemplate obj = null;

		reply = Console.request("Number of object from 1-5: ", "^[1-5]$");

		try {
			obj = (ObjectTemplate) Class.forName(
					Main.class.getPackage().getName() + ".Object" + reply)
					.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		if (obj != null) {
			obj.create();
		}

		new A3.ObjectInspector().inspect(obj, true);

		try {
			Document serialized = new Serializer().serialize(TestableComplexObject.TEST_OBJ);
			String xmlStr = new XMLOutputter().outputString(serialized);
			System.out.println(new XMLOutputter(org.jdom.output.Format.getPrettyFormat()).outputString(serialized));

			SAXBuilder builder = new SAXBuilder();
			Document anotherDocument = builder.build(new ByteArrayInputStream(
					xmlStr.getBytes("UTF-8")));

			Object result = new Deserializer().deserialize(anotherDocument);
			new A3.ObjectInspector().inspect(result, true);

			System.out.println(TestableComplexObject.TEST_OBJ);
			System.out.println(result);
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
