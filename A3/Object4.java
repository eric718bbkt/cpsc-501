package A3;

/*========================================================================
 File: Object4.java
 Author: Eric Kumlin
 Date: November 2014
 ========================================================================*/

import java.util.ArrayList;
import java.util.List;

public class Object4 implements ObjectTemplate {
	Object1[] objects;

	public Object4() {
		objects = new Object1[0];
	}

	public void create() {
		String s;

		List<Object1> objs = new ArrayList<Object1>();
		System.out
				.println("Please enter \"add\" to add another object, or anything else to finish:");
		while (true) {
			s = Console.request("Command: ", "^.*$");

			if (!"add".equals(s)) {
				break;
			}

			Object1 obj = new Object1();
			obj.create();
			objs.add(obj);
		}

		objects = objs.toArray(new Object1[0]);
	}
}
