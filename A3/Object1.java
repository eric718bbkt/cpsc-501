package A3;

/*========================================================================
 File: Object1.java
 Author: Eric Kumlin
 Date: November 2014
 ========================================================================*/

public class Object1 implements ObjectTemplate {
	int a;
	float b;
	double c;
	char d;
	byte e;
	short f;
	boolean g;
	String h;

	public Object1() {
		h = "";
	}

	public void create() {
		a = Integer.parseInt(Console.request("Please enter an integer: ",
				"^-?[0-9]+$"));
		b = Float.parseFloat(Console.request("Please enter a float: ",
				"^-?[0-9.]+$"));
		c = Double.parseDouble(Console.request("Please enter a double: ",
				"^-?[0-9.]+$"));
		d = Console.request("Please enter a character: ", "^.$").charAt(0);
		e = Byte.parseByte(Console.request("Please enter a byte: ",
				"^-?[0-9]+$"));
		f = Short.parseShort(Console.request("Please enter a short: ",
				"^-?[0-9]+$"));
		g = Boolean.parseBoolean(Console.request("Please enter a boolean: ",
				"^(true|false|0|1)$"));
		h = Console.request("Please enter any text: ", "^.*$");
	}
}
