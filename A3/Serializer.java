package A3;

/*========================================================================
 File: Serializer.java
 Author: Eric Kumlin
 Date: November 2014
 ========================================================================*/

import java.lang.reflect.Array;
import java.lang.reflect.Field;

import org.jdom.Document;
import org.jdom.Element;

public class Serializer {
	private Element root;

	public Serializer() {
		root = new Element("serialized");
	}

	public Document serialize(Object obj) {
		Document doc = new Document();
		doc.setRootElement(root);

		Element baseObjectElement = serializeObjectToRoot(obj, root);
		baseObjectElement.setAttribute("primary", "true");

		return doc;
	}

	private void serializeFields(Object obj, Element eObject) {
		Class<?> objClass = obj.getClass();

		for (Field f : objClass.getDeclaredFields()) {
			f.setAccessible(true);

			Element eField = new Element("field");

			Object value = null;
			try {
				value = f.get(obj);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

			serializeValue(value, eField);

			String fieldName = f.getName();
			String fieldClass = f.getDeclaringClass().getName();

			eField.setAttribute("name", fieldName);
			eField.setAttribute("declaringclass", fieldClass);

			eObject.addContent(eField);
		}
	}

	private Element serializeObjectToRoot(Object obj, Element root) {
		String id = Integer.toString(obj.hashCode());
		for (Object elemAsObj : root.getChildren()) {
			Element elem = (Element) elemAsObj;
			if (elem.getAttributeValue("id").equals(id)) {
				return elem;
			}
		}

		Class<?> objClass = obj.getClass();

		Element eObject = new Element("object");
		eObject.setAttribute("class", objClass.getName());
		eObject.setAttribute("id", id);
		root.addContent(eObject);

		if (objClass.isArray()) {
			int length = Array.getLength(obj);
			for (int i = 0; i < length; i++) {
				Object value = Array.get(obj, i);

				serializeValue(value, eObject);
			}
		} else {
			serializeFields(obj, eObject);
		}

		return eObject;
	}

	private void serializeValue(Object value, Element eField) {
		if (value == null) {
			eField.addContent(new Element("null"));
		} else if (ObjectInspector.WRAPPER_TYPES.contains(value.getClass())) {
			Element eValue = new Element("value");
			eValue.addContent(value.toString());
			eField.addContent(eValue);
		} else {
			Element eValue = new Element("reference");
			Element refObjectElement = serializeObjectToRoot(value, root);
			int referenceId = Integer.parseInt(refObjectElement
					.getAttributeValue("id"));

			eValue.addContent(Integer.toString(referenceId));
			eField.addContent(eValue);
		}
	}
}
