package A3;

/*========================================================================
 File: ObjectInspector.java
 ========================================================================*/

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ObjectInspector {
	public static final int NESTING_LIMIT = 12;
	public static final Set<Class<?>> WRAPPER_TYPES = new HashSet<Class<?>>(
			Arrays.asList(Boolean.class, Character.class, Byte.class,
					Short.class, Integer.class, Long.class, Float.class,
					Double.class, Void.class, String.class));

	private static Set<Object> inspectedObjects = new HashSet<Object>();

	public ObjectInspector() {
	}

	public void inspect(Object obj, boolean recursive) {
		inspectedObjects.clear();
		inspect(obj, recursive, 0);
	}

	private void inspect(Object obj, boolean recursive, int tabCount) {
		if (obj == null || obj.getClass() == null) {
			System.out.println("Object is null");
			return;
		}

		if (WRAPPER_TYPES.contains(obj.getClass())) {
			System.out.println(repeatString("\t", tabCount) + obj);
			return;
		}

		if (tabCount > NESTING_LIMIT) {
			throw new RuntimeException("Too much nesting");
		}

		System.out.println();

		if (inspectedObjects.contains(obj)) {
			System.out
					.println(repeatString("\t", tabCount)
							+ "This object has already been inspected; circular reference detected");
			return;
		}

		inspectedObjects.add(obj);

		System.out.println(repeatString("\t", tabCount) + "Declaring class: "
				+ obj.getClass().getSimpleName());

		System.out.println(repeatString("\t", tabCount)
				+ "Immediate superclass: "
				+ (obj.getClass().getSuperclass() == null ? "null" : obj
						.getClass().getSuperclass().getSimpleName()));

		System.out.println(repeatString("\t", tabCount) + "Interfaces list: "
				+ classAsString(obj.getClass().getInterfaces()));

		List<Class<?>> allAncestors = new ArrayList<Class<?>>(
				getAllAncestors(obj.getClass()));

		System.out.println(repeatString("\t", tabCount) + "Methods:");
		for (Class<?> tobjClass : allAncestors) {
			for (Method m : tobjClass.getDeclaredMethods()) {
				System.out.printf(
						repeatString("\t", tabCount)
								+ "\t%s %s %s.%s(%s) throws %s",
						Modifier.toString(m.getModifiers()),
						m.getReturnType().getSimpleName(),
						tobjClass.getSimpleName(),
						m.getName(),
						classAsString(m.getParameterTypes()),
						m.getExceptionTypes().length > 0 ? classAsString(m
								.getExceptionTypes()) : "no exceptions");
				System.out.println();
			}
		}

		System.out.println(repeatString("\t", tabCount) + "Constructors:");
		for (Class<?> tobjClass : allAncestors) {
			for (Constructor<?> m : tobjClass.getDeclaredConstructors()) {
				System.out.printf(repeatString("\t", tabCount) + "\t%s %s(%s)",
						Modifier.toString(m.getModifiers()),
						tobjClass.getSimpleName(),
						classAsString(m.getParameterTypes()));
				System.out.println();
			}
		}

		System.out.println(repeatString("\t", tabCount) + "Fields:");
		for (Class<?> tobjClass : allAncestors) {
			for (Field m : tobjClass.getDeclaredFields()) {
				System.out.printf(repeatString("\t", tabCount)
						+ "\t%s %s %s.%s", Modifier.toString(m.getModifiers()),
						m.getType().getSimpleName(), tobjClass.getSimpleName(),
						m.getName());

				try {
					m.setAccessible(true);
					Object value = m.get(obj);
					if (value == null) {
						System.out.printf(" = null");
					} else if (!recursive
							|| WRAPPER_TYPES.contains(value.getClass())
							|| value.getClass() == String.class) {
						System.out.printf(" = %s", value);
					} else if (value.getClass().isArray()) {
						System.out.println(" = object array:");

						int length = Array.getLength(value);
						for (int i = 0; i < length; i++) {
							System.out.printf(repeatString("\t", tabCount)
									+ "\tElement " + i + ": ");
							inspect(Array.get(value, i), recursive,
									tabCount + 2);
						}
					} else {
						System.out.printf(" = object: ");
						inspect(value, recursive, tabCount + 2);
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.printf(" (unable to fetch value)");
				}
				System.out.println();
			}
		}
	}

	private static String classAsString(Class<?>[] classes) {
		StringBuilder sb = new StringBuilder();

		boolean isFirst = true;
		for (Class<?> c : classes) {
			if (!isFirst) {
				sb.append(", ");
			}
			isFirst = false;

			sb.append(c.getCanonicalName());
		}

		return sb.toString();
	}

	private Set<Class<?>> getAllAncestors(Class<?> tobjClass) {
		Set<Class<?>> result = new HashSet<Class<?>>();
		if (tobjClass == null) {
			return result;
		}

		while (tobjClass != null) {
			result.add(tobjClass);
			result.addAll(getAllAncestors(tobjClass.getSuperclass()));
			result.addAll(Arrays.asList(tobjClass.getInterfaces()));

			tobjClass = tobjClass.getSuperclass();
		}

		return result;
	}

	private String repeatString(String str, int times) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < times; i++)
			sb.append(str);
		return sb.toString();
	}
}
