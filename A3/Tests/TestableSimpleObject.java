package A3.Tests;

/*========================================================================
 File: TestableSimpleObject.java
 Author: Eric Kumlin
 Date: November 2014
 ========================================================================*/

public class TestableSimpleObject {
	public static final TestableSimpleObject TEST_OBJ;

	public int a = 1234;

	static {
		TEST_OBJ = new TestableSimpleObject();
	}
}
