package A3.Tests;

/*========================================================================
 File: TestableComplexObject.java
 Author: Eric Kumlin
 Date: November 2014
 ========================================================================*/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestableComplexObject {
	public static final TestableComplexObject TEST_OBJ;
	
	public String x;
	public float a1 = 5.6f;
	public int a2 = 3;
	public boolean a3 = true;
	public double a4 = 6.5;
	public long a5 = 5L;
	public Object a6 = null;
	public int[] a7 = { 1, 2, 3 };
	public Object[] a8 = { "x" };
	public char a9 = 'a';
	public List<TestableComplexObject> idks;
	
	static {
		TEST_OBJ = new TestableComplexObject("6");
		TEST_OBJ.a1 = 1.337f;
		TEST_OBJ.a2 = 1;
		TEST_OBJ.a3 = false;
		TEST_OBJ.a4 = 3.1173;
		TEST_OBJ.a5 = 1L;
		TEST_OBJ.a7 = new int[] { 4, 5, 6 };
		TEST_OBJ.a8 = new Object[] { "new" };
		TEST_OBJ.a9 = 'b';
	}

	public TestableComplexObject() {
		this("5");
	}

	public TestableComplexObject(String y) {
		idks = new ArrayList<TestableComplexObject>();
		x = y;
		if ("6".equals(y)) {
			a6 = new TestableComplexObject();
			idks.add(new TestableComplexObject("7"));
			idks.add(new TestableComplexObject("8"));
		}
	}

	public String toString() {
		return String.format("{ %s; %f; %d; %s; %f; %d; %s; %s; %s; %c; %s }",
				x, a1, a2, a3 ? "true" : "false", a4, a5, a6,
				Arrays.toString(a7), Arrays.toString(a8), a9, idks);
	}
}
