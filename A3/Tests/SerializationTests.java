package A3.Tests;

/*========================================================================
 File: SerializationTests.java
 Author: Eric Kumlin
 Date: November 2014
 ========================================================================*/

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.junit.Assert;
import org.junit.Test;

import A3.Deserializer;
import A3.Serializer;

public class SerializationTests {
	@Test
	public void testSerialize() {
		Object obj = TestableComplexObject.TEST_OBJ;

		Document doc = new Serializer().serialize(obj);
		Element root = doc.getRootElement();

		// 17 nested objects total
		Assert.assertEquals(17, root.getChildren("object").size());

		// Find base element
		Element baseElem = null;
		for (Object asObj : root.getChildren("object")) {
			Element elem = (Element) asObj;
			if ("true".equals(elem.getAttributeValue("primary"))) {
				if (baseElem == null) {
					baseElem = elem;
				} else {
					Assert.fail("Too many base elements");
				}
			}
		}

		// We need a base element
		Assert.assertNotNull(baseElem);

		// 12 fields total
		Assert.assertEquals(12, baseElem.getChildren("field").size());
	}

	@Test
	public void testDeserialize() throws UnsupportedEncodingException,
			JDOMException, IOException {
		SAXBuilder builder = new SAXBuilder();
		Document anotherDocument = builder
				.build(new ByteArrayInputStream(
						"<serialized><object class=\"A3.Tests.TestableSimpleObject\" primary=\"true\"><field name=\"a\"><value>5678</value></field></object></serialized>"
								.getBytes("UTF-8")));

		TestableSimpleObject result = (TestableSimpleObject) new Deserializer()
				.deserialize(anotherDocument);

		Assert.assertEquals(5678, result.a);
	}

	@Test
	public void testEntireProcess() throws UnsupportedEncodingException,
			JDOMException, IOException {
		Document serialized = new Serializer().serialize(TestableComplexObject.TEST_OBJ);
		String xmlStr = new XMLOutputter().outputString(serialized);

		SAXBuilder builder = new SAXBuilder();
		Document anotherDocument = builder.build(new ByteArrayInputStream(
				xmlStr.getBytes("UTF-8")));

		Object result = new Deserializer().deserialize(anotherDocument);
		
		String originalObj = TestableComplexObject.TEST_OBJ.toString();
		String newObj = result.toString();
		
		Assert.assertEquals(originalObj, newObj);
	}
}
