package A3;

/*========================================================================
 File: Console.java
 Author: Eric Kumlin
 Date: November 2014
 ========================================================================*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {
	private static BufferedReader br;

	public static String request(String prompt, String regex) {
		if (br == null) {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String s;
		do {
			System.out.printf(prompt);
			try {
				s = br.readLine();
			} catch (IOException e) {
				s = "";
			}
		} while (!s.matches(regex));
		
		return s;
	}
}
