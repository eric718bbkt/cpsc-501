package A3;

/*========================================================================
 File: Object3.java
 Author: Eric Kumlin
 Date: November 2014
 ========================================================================*/

import java.util.ArrayList;
import java.util.List;

public class Object3 implements ObjectTemplate {
	int[] a;
	String[] b;

	public Object3() {
		a = new int[0];
		b = new String[0];
	}

	public void create() {
		String s;

		List<Integer> aNums = new ArrayList<Integer>();
		System.out
				.println("Please enter any number of integers, followed by a blank line when done:");
		while (true) {
			s = Console.request("Integer: ", "^-?[0-9]*$");

			if ("".equals(s)) {
				break;
			}

			aNums.add(Integer.parseInt(s));
		}

		a = new int[aNums.size()];
		for (int i = 0; i < aNums.size(); i++) {
			a[i] = aNums.get(i);
		}

		List<String> bStrs = new ArrayList<String>();
		System.out
				.println("Please enter any number of strings, followed by a blank line when done:");
		while (true) {
			s = Console.request("String: ", "^.*$");

			if ("".equals(s)) {
				break;
			}

			bStrs.add(s);
		}

		b = bStrs.toArray(new String[0]);
	}
}
