package A3;

/*========================================================================
 File: Object2.java
 Author: Eric Kumlin
 Date: November 2014
 ========================================================================*/

public class Object2 implements ObjectTemplate {
	String a;
	Object1 b;

	public Object2() {
		b = null;
	}

	public void create() {
		a = Console.request("Please enter any text: ", "^.*$");

		System.out.println("You will now create a new Object1 object:");
		b = new Object1();
		b.create();
		System.out.println("Created new Object1 object!");
	}
}
