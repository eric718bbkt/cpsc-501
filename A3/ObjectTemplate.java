package A3;

/*========================================================================
 File: ObjectTemplate.java
 Author: Eric Kumlin
 Date: November 2014
 ========================================================================*/

public interface ObjectTemplate {
	public void create();
}
