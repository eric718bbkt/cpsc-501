package A3;

/*========================================================================
 File: Object5.java
 Author: Eric Kumlin
 Date: November 2014
 ========================================================================*/

import java.util.ArrayList;
import java.util.List;

public class Object5 implements ObjectTemplate {
	List<Object1> objects;

	public Object5() {
		objects = new ArrayList<Object1>();
	}

	public void create() {
		String s;

		System.out
				.println("Please enter \"add\" to add another object, or anything else to finish:");
		while (true) {
			s = Console.request("Command: ", "^.*$");

			if (!"add".equals(s)) {
				break;
			}

			Object1 obj = new Object1();
			obj.create();
			objects.add(obj);
		}
	}
}
