package A3;

/*========================================================================
 File: Sender.java
 Author: Eric Kumlin
 Date: November 2014
 ========================================================================*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.jdom.Document;
import org.jdom.output.XMLOutputter;

public class Sender {
	public static void main(String[] args) throws IOException {
		String host = Console.request("Please enter the server name to use: ",
				"^.*$");
		int port = Integer.parseInt(Console.request(
				"Please enter the port number to use: ", "^[0-9]{3,5}$"));

		Socket socket = new Socket(host, port);
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

			String reply = "";

			ObjectTemplate obj = null;

			while (obj == null) {
				reply = Console.request("Number of object from 1-5: ",
						"^[1-5]$");

				try {
					obj = (ObjectTemplate) Class.forName(
							Sender.class.getPackage().getName() + ".Object"
									+ reply).newInstance();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}

				obj.create();
			}

			Document serialized = new Serializer().serialize(obj);
			String xmlStr = new XMLOutputter().outputString(serialized);

			out.println(xmlStr.replaceAll("[\r\n]", ""));

			String answer = in.readLine();
			System.out.println(answer);
		} finally {
			socket.close();
		}

		System.exit(0);
	}
}
