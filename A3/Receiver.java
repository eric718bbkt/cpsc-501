package A3;

/*========================================================================
 File: Receiver.java
 Author: Eric Kumlin
 Date: November 2014
 ========================================================================*/

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class Receiver {
	public static void main(String[] args) throws IOException, JDOMException {
		int port = Integer.parseInt(Console.request(
				"Please enter the port number to use (3-5 digits): ",
				"^[0-9]{3,5}$"));

		ServerSocket listener = new ServerSocket(port);

		try {
			while (true) {
				Socket socket = listener.accept();
				try {
					BufferedReader in = new BufferedReader(
							new InputStreamReader(socket.getInputStream()));
					PrintWriter out = new PrintWriter(socket.getOutputStream(),
							true);

					while (true) {
						String xmlStr = in.readLine();

						if (xmlStr == null) {
							// Terminated the connection
							break;
						}

						System.out.println("Received XML string from client:");
						System.out.println(xmlStr);

						SAXBuilder builder = new SAXBuilder();
						Document anotherDocument = builder
								.build(new ByteArrayInputStream(xmlStr
										.getBytes("UTF-8")));

						Object result = new Deserializer()
								.deserialize(anotherDocument);

						new A3.ObjectInspector().inspect(result, true);
						out.println("Object received and inspected");
					}
				} finally {
					socket.close();
				}
			}
		} finally {
			listener.close();
		}
	}
}
