#ifndef WAVFILE_H
#define WAVFILE_H

#include <algorithm>
#include <iterator>
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>

#include "FileUtil.h"
#include "AudioUtil.h"
#include "AudioFile.h"

class WavFile : AudioFile {
public:
	static const int RIFF_HEADER = 0x46464952; // Since we pull ints backwards, we have to use the reverse of the bytes "52 49 46 46" for "RIFF"
	static const int WAVE_HEADER = 0x45564157; // Since we pull ints backwards, we have to use the reverse of the bytes "57 41 56 45" for "WAVE"
	static const int FMT_HEADER = 0x20746D66;  // Since we pull ints backwards, we have to use the reverse of the bytes "66 6D 74 20" for "fmt "
	static const int DATA_HEADER = 0x61746164; // Since we pull ints backwards, we have to use the reverse of the bytes "64 61 74 61" for "data"
	static const int DEFAULT_SC1SIZE = 16;
	static const int DEFAULT_SC2DIFF = DEFAULT_SC1SIZE + 20;

	// Order and datatypes taken from "The Canonical WAVE file format" document
	// skip chunkId
	int chunkSize;
	// skip format
	// skip subchunk1Id
	int subchunk1Size;
	short audioFormat;
	short numChannels;
	int sampleRate;
	int byteRate;
	short blockAlign;
	short bitsPerSample;
	// skip subchunk2Id
	int subchunk2Size;
	// Done; remainder is data

	std::vector<short> data;

	WavFile() {};
	WavFile(WavFile*);
	void convolve(WavFile*, WavFile*);
	void load(std::ifstream&);
	void readHeader(std::vector<char>&, int&);
	void rehash();
	void save(std::ofstream&);
};

#endif
