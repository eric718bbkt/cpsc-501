#ifndef REGRESSIONTESTS_H
#define REGRESSIONTESTS_H

#include <vector>
#include <iostream>
#include <stdio.h>
#include <cmath>
#include <limits>

#include "AudioUtil.h"

class RegressionTests {
public:
	static void run();
	static bool testComplexConvolve();
	static bool testFour1();

private:
	static bool displayTest(char*, bool);
};

#endif
