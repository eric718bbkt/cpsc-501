#include "FileUtil.h"

/**
 * Checks whether a file exists and is accessible.
 *
 * @param filePath The path of the file to check.
 * @return true if the file exists and can be accessed, false otherwise.
 */
bool FileUtil::exists(char* filePath) {
	struct stat buffer;
	return (stat(filePath, &buffer) == 0);
}

/**
 * Reads a file stream into a char vector.
 *
 * @param f The input file stream to read from.
 * @return A vector of chars representing the data.
 */
std::vector<char> FileUtil::read(std::ifstream& f) {
	std::vector<char> bytes;

	f.seekg(0, std::ios::end);
	std::ifstream::pos_type filesize = f.tellg();
	f.seekg(0, std::ios::beg);

	if (filesize <= 0) {
		return bytes;
	}

	bytes.resize(int(filesize));
	f.read(&bytes[0], filesize);

	return bytes;
}

/**
 * Reads an integer from a binary file.
 *
 * @param data The vector character stream to read data from.
 * @param offset The offset to start at. Will be incremented by the size of the returned data after function returns.
 */
int FileUtil::pullIntData(std::vector<char>& data, int& offset) {
	int length = sizeof(int);

	if (int(data.size()) < offset + length) {
		return INT_MIN;
	}

	int result = 0;
	for (int i = 0; i < length; i++) {
		result += ((unsigned char)data[offset + i]) << (i * 8);
	}

	offset += length;

	return result;
}

/**
 * Reads a short from a binary file.
 *
 * @param data The vector character stream to read data from.
 * @param offset The offset to start at. Will be incremented by the size of the returned data after function returns.
 */
short FileUtil::pullShortData(std::vector<char>& data, int& offset) {
	int length = sizeof(short);

	if (int(data.size()) < offset + length) {
		return SHRT_MIN;
	}

	short result = 0;
	for (int i = 0; i < length; i++) {
		result += ((unsigned char)data[offset + i]) << (i * 8);
	}

	offset += length;

	return result;
}

/**
 * Writes a char to a binary file.
 *
 * @param f The output file stream to write to.
 * @param data The data to write to the file.
 */
void FileUtil::write(std::ofstream& f, char data) {
	f << data;
}

/**
 * Writes an int to a binary file.
 *
 * @param f The output file stream to write to.
 * @param data The data to write to the file.
 */
void FileUtil::write(std::ofstream& f, int data) {
	int length = sizeof(data);

	for (int i = 0; i < length; i++) {
		FileUtil::write(f, (char)((data >> (i * 8)) & 0xFF));
	}
}

/**
 * Writes a short to a binary file.
 *
 * @param f The output file stream to write to.
 * @param data The data to write to the file.
 */
void FileUtil::write(std::ofstream& f, short data) {
	int length = sizeof(data);

	for (int i = 0; i < length; i++) {
		FileUtil::write(f, (char)((data >> (i * 8)) & 0xFF));
	}
}
