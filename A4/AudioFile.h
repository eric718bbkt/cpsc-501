#ifndef AUDIOFILE_H
#define AUDIOFILE_H

#include <fstream>
#include <vector>

class AudioFile {
public:
	virtual void load(std::ifstream&) = 0;
	virtual void save(std::ofstream&) = 0;

	int channels;
	int bitsPerSample;
	int sampleRate;

	int max;
	int min;
};

#endif
