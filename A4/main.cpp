#include <time.h>
#include <stdio.h>
#include <fstream>
#include <vector>

#include "RegressionTests.h"
#include "WavFile.h"

#define TEST // Uncomment me to run tests

int main(int argc, char** argv) {
#ifdef TEST
	RegressionTests::run();
#endif

	int returnCode = EXIT_SUCCESS;

	if (argc >= 4) {
		char* inputFile = argv[1];
		char* irFile = argv[2];
		char* outputFile = argv[3];

		if (!FileUtil::exists(inputFile)) {
			printf("Specified input file '%s' does not exist\n", inputFile);
			returnCode = EXIT_FAILURE;
		} else if (!FileUtil::exists(inputFile)) {
			printf("Specified input file '%s' does not exist\n", inputFile);
			returnCode = EXIT_FAILURE;
		} else {
			printf("Loading in dry file...\n");

			std::ifstream dryLoader(inputFile, std::ios::binary);
			WavFile* dry = new WavFile();
			dry->load(dryLoader);
			dryLoader.close();

			printf("Loading in IR file...\n");

			std::ifstream irLoader(irFile, std::ios::binary);
			WavFile* ir = new WavFile();
			ir->load(irLoader);
			irLoader.close();

			printf("Convolving...\n");

			clock_t before = clock();

			WavFile* convolved = new WavFile(dry);
			dry->convolve(ir, convolved);

			double elapsed = clock() - before;

			printf("Convolution time: %.3fsec\n", elapsed / 1000.0);

			printf("Writing output...\n");

			std::ofstream saver(outputFile, std::ios::binary);
			convolved->save(saver);
			saver.close();

			printf("Done\n");
		}
	} else {
		printf("Usage: Convoluted <inputfile> <irfile> <outputfile>\n");
		returnCode = EXIT_FAILURE;
	}

	printf("Hit 'Enter' to terminate...\n");
	getchar();

	return returnCode;
}
