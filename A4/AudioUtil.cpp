#include "AudioUtil.h"

/**
 * Performs a complex convolution on the frequency domain.
 *
 * @param dry The signal to transform.
 * @param ir The signal of the impulse response to transform with.
 * @param result A vector in which to store the transformed signal.
 */
void AudioUtil::complexConvolve(std::vector<double>& dry, std::vector<double>& ir, std::vector<double>& result) {
	unsigned int length = result.size();

	for (unsigned int u = 0; u < length; u += 2) {
		unsigned int uv = u + 1;
		double
			currentDryVal = dry[u],
			nextDryVal = dry[uv],
			currentIrVal = ir[u],
			nextIrVal = ir[uv];

		result[u] = currentDryVal * currentIrVal - nextDryVal * nextIrVal;
		result[uv] = currentDryVal * nextIrVal + nextDryVal * currentIrVal;
	}
}

/**
 * Converts a wave-ready data array of shorts to a double-signal.
 *
 * @param data The data to transform into a signal.
 * @param minValue The minimum data point in the data.
 * @param result A vector in which to store the resulting signal.
 */
void AudioUtil::convertDataToSignal(std::vector<short>& data, int minValue, std::vector<double>& result) {
	unsigned int length = data.size();
	result.resize(length);

	double minValueDouble = double(minValue);
	for (unsigned int u = 0; u < length; u++) {
		result[u] = double(data[u]) / minValueDouble;
	}
}

/**
 * Converts a signal to be complex.
 *
 * @param realSignal The real signal to act as a source.
 * @param complexSignal The complex signal result.
 */
void AudioUtil::convertSignalToComplex(std::vector<double>& realSignal, std::vector<double>& complexSignal) {
	unsigned int realLength = realSignal.size();
	complexSignal.resize(realLength << 1);

	for (unsigned int u = 0; u < realLength; u++) {
		unsigned int v = u << 1;
		complexSignal[v] = realSignal[u];
		complexSignal[v + 1] = 0.0;
	}
}

/**
 * Converts a double-signal to a wave-ready data array of shorts.
 *
 * @param signal The signal to act as a source.
 * @param maxValue The maximum value of data points in the original data.
 * @param result A vector in which to store the data from the signal.
 * @param isComplex true if the signal is complex, false otherwise.
 */
void AudioUtil::convertSignalToData(std::vector<double>& signal, int maxValue, std::vector<short>& result, bool isComplex) {
	unsigned int length = signal.size();
	result.resize(length);

	double maxValueDouble = double(maxValue);
	if (isComplex) {
		for (unsigned int u = 0; u < length; u += 4) {
			unsigned int v = u >> 1;
			result[v] = short(signal[u] * maxValueDouble);
			result[v + 1] = short(signal[u + 2] * maxValueDouble);
		}
	} else {
		for (unsigned int u = 0; u < length; u++) {
			result[u] = short(signal[u] * maxValueDouble);
		}
	}
}

/**
 * Performs a convolution on the input signal.
 *
 * @param method One of the METHOD_ constants in the AudioUtil class representing the convolution method to use.
 * @param dry The signal to transform.
 * @param ir The signal of the impulse response to transform with.
 * @param result A vector in which to store the transformed signal.
 */
void AudioUtil::convolve(const int method, std::vector<double>& dry, std::vector<double>& ir, std::vector<double>& result) {
	if (method == AudioUtil::METHOD_BASELINE) {
		AudioUtil::convolveMethod1(dry, ir, result);
	} else if (method == AudioUtil::METHOD_FFT) {
		AudioUtil::convolveMethod2(dry, ir, result);
	}
}

/**
 * Performs a slow time-domain-based convolution on the input signal.
 *
 * @param dry The signal to transform.
 * @param ir The signal of the impulse response to transform with.
 * @param result A vector in which to store the transformed signal.
 */
void AudioUtil::convolveMethod1(std::vector<double>& dry, std::vector<double>& ir, std::vector<double>& result) {
	unsigned int N = dry.size(), M = ir.size();
	unsigned int length = N + M - 1;
	result.resize(length);

	for (unsigned int u = 0; u < length; u++) {
		result[u] = 0.0;
	}

	for (unsigned int n = 0; n < N; n++) {
		double currentDry = dry[n];
		for (unsigned int m = 0; m < M; m++) {
			result[n + m] += currentDry * ir[m];
		}
	}
}

/**
 * Performs a quick FFT-based convolution on the input signal.
 *
 * @param dry The signal to transform.
 * @param ir The signal of the impulse response to transform with.
 * @param result A vector in which to store the transformed signal.
 */
void AudioUtil::convolveMethod2(std::vector<double>& dry, std::vector<double>& ir, std::vector<double>& result) {
	unsigned int N = dry.size(), M = ir.size();
	unsigned int length = N + M - 1;

	std::vector<double> dryTemp, irTemp;

	unsigned int powerAlignedLength = 1;
	while (powerAlignedLength < length) {
		powerAlignedLength <<= 1;
	}

	unsigned int extendedLength = powerAlignedLength << 1;

	AudioUtil::convertSignalToComplex(dry, dryTemp);
	dryTemp.resize(extendedLength, 0.0);
	AudioUtil::fourOneFft(dryTemp, false);

	AudioUtil::convertSignalToComplex(ir, irTemp);
	irTemp.resize(extendedLength, 0.0);
	AudioUtil::fourOneFft(irTemp, false);

	result.resize(extendedLength, 0.0);

	AudioUtil::complexConvolve(dryTemp, irTemp, result);
	AudioUtil::fourOneFft(result, true);

	AudioUtil::postProcessComplex(result);
}

/**
 * Performs a four1 FFT from Numerical Recipes in C, p.507 - p.508. Based on code provided by Leonard Manzara.
 * Modified to take a vector as input instead of an array, and to be zero-based instead of one-based.
 *
 * @param signal A vector representing the signal to process.
 * @param invert true if the four1 algorithm should be inverted, false otherwise.
 */
void AudioUtil::fourOneFft(std::vector<double>& signal, bool invert) {
	int isign = invert ? -1 : 1;
	unsigned int n = signal.size(), nn = n >> 1;

	unsigned long mmax, m, j, istep, i;
	double wtemp, wr, wpr, wpi, wi, theta;
	double tempr, tempi;

	j = 1;

	for (i = 1; i < n; i += 2) {
		if (j > i) {
			wtemp = signal[i - 1];
			signal[i - 1] = signal[j - 1];
			signal[j - 1] = wtemp;

			wtemp = signal[i];
			signal[i] = signal[j];
			signal[j] = wtemp;
		}

		m = nn;
		while (m >= 2 && j > m) {
			j -= m;
			m >>= 1;
		}

		j += m;
	}

	mmax = 2;
	while (n > mmax) {
		istep = mmax << 1;
		theta = isign * (6.28318530717959 / mmax);
		wtemp = sin(0.5 * theta);
		wpr = -2.0 * wtemp * wtemp;
		wpi = sin(theta);
		wr = 1.0;
		wi = 0.0;
		for (m = 1; m < mmax; m += 2) {
			for (i = m; i <= n; i += istep) {
				j = i + mmax;
				unsigned long iv = i - 1, jv = j - 1;
				double
					currentJSignalVal = signal[jv],
					nextJSignalVal = signal[j];

				tempr = wr * currentJSignalVal - wi * nextJSignalVal;
				tempi = wr * nextJSignalVal + wi * currentJSignalVal;
				signal[jv] = signal[iv] - tempr;
				signal[j] = signal[i] - tempi;
				signal[iv] += tempr;
				signal[i] += tempi;
			}
			wr = (wtemp = wr) * wpr - wi * wpi + wr;
			wi = wi * wpr + wtemp * wpi + wi;
		}
		mmax = istep;
	}
}

/**
 * Converts a value from a range with a defined max and min, to be applied to a new range with a new defined max and min.
 * Max and min are represented as upper and lower bounds, respectively.
 *
 * @param value The actual value to convert.
 * @param lowerBound The minimum value of the initial range.
 * @param upperBound The maximum value of the initial range.
 * @param newLowerBound The minimum value of the new range.
 * @param newUpperBound The maximum value of the new range.
 * @return A new value based on the shift of the given value.
 */
double AudioUtil::normalize(double value, double lowerBound, double upperBound, double newLowerBound, double newUpperBound) {
	return ((newLowerBound - upperBound) * (value - lowerBound) / (upperBound - lowerBound)) + newLowerBound;
}

/**
 * Run post-processing normalization on a complex signal output after four1 transformation.
 * If this is not run, output may consist of static.
 *
 * @param signal The signal to perform normalization on.
 */
void AudioUtil::postProcessComplex(std::vector<double>& signal) {
	unsigned int extendedLength = signal.size();
	double min = FLT_MAX, max = FLT_MIN, extendedLengthDouble = double(extendedLength);

	for (unsigned int u = 0; u < extendedLength; u++) {
		double newValue = signal[u] / extendedLengthDouble;
		signal[u] = newValue;

		if (newValue < min) {
			min = newValue;
		} else if (newValue > max) {
			max = newValue;
		}
	}

	for (unsigned int u = 0; u < extendedLength; u++) {
		signal[u] = AudioUtil::normalize(signal[u], min, max, -1.0, 1.0);
	}
}
