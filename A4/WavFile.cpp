#include "WavFile.h"

/**
 * Constructs a WavFile object with the same header data as another.
 *
 * @param tmplate The WavFile object to copy header data from.
 */
WavFile::WavFile(WavFile* tmplate) {
	this->audioFormat = tmplate->audioFormat;
	this->numChannels = tmplate->numChannels;
	this->sampleRate = tmplate->sampleRate;
	this->byteRate = tmplate->byteRate;
	this->blockAlign = tmplate->blockAlign;
	this->bitsPerSample = tmplate->bitsPerSample;
}

void WavFile::convolve(WavFile* response, WavFile* result) {
	unsigned int
		N = this->data.size(),
		M = response->data.size();
	std::vector<double> dry(N), ir(M), output, drySignal, irSignal;
	std::vector<short> convolved;

	AudioUtil::convertDataToSignal(this->data, abs(this->min), drySignal);
	AudioUtil::convertDataToSignal(response->data, abs(response->min), irSignal);

#ifdef FFT
	AudioUtil::convolve(AudioUtil::METHOD_FFT, drySignal, irSignal, output);
	AudioUtil::convertSignalToData(output, abs(this->max), convolved, true);

	convolved.resize((N + M) * sizeof(short) + 1);
#else
	AudioUtil::convolve(AudioUtil::METHOD_BASELINE, drySignal, irSignal, output);
	AudioUtil::convertSignalToData(output, abs(this->max), convolved, false);
#endif

	result->data = convolved;
	result->rehash();
}

/**
 * Loads header data and sound data from a given file.
 *
 * @param f The file stream to load data from.
 */
void WavFile::load(std::ifstream& f) {
	std::vector<char> fileData = FileUtil::read(f);
	int offset = 0;

	this->readHeader(fileData, offset);

	if (this->sampleRate != 44100) {
		printf("Invalid sample rate\n");
		return;
	}

	if (this->bitsPerSample != 16) {
		printf("Invalid bits per sample\n");
		return;
	}

	int sampleCount = this->subchunk2Size / sizeof(short);
	this->data.resize(sampleCount);
	for (int i = 0; i < sampleCount; i++) {
		short x = FileUtil::pullShortData(fileData, offset);
		this->data[i] = x;
	}

	this->min = -(int)pow(2.0, this->bitsPerSample - 1);
	this->max = (int)pow(2.0, this->bitsPerSample - 1) - 1;
}

/**
 * Parses the header of a data stream loaded from a file.
 *
 * @param fileData The vector character stream to read data from.
 * @param offset The offset to start at. Will be incremented to the end of the header data after function returns.
 */
void WavFile::readHeader(std::vector<char>& fileData, int& offset) {
	int chunkId = FileUtil::pullIntData(fileData, offset);
	if (chunkId != WavFile::RIFF_HEADER) {
		printf("Failed to load file; header does not have RIFF chunk ID\n");
		return;
	}

	this->chunkSize = FileUtil::pullIntData(fileData, offset);

	int format = FileUtil::pullIntData(fileData, offset);
	if (format != WavFile::WAVE_HEADER) {
		printf("Failed to load file; header does not have WAVE format\n");
		return;
	}

	int sc1id = FileUtil::pullIntData(fileData, offset);
	if (sc1id != WavFile::FMT_HEADER) {
		printf("Failed to load file; subchunk 1 does not have 'fmt ' tag\n");
		return;
	}

	this->subchunk1Size = FileUtil::pullIntData(fileData, offset);
	int subchunk1End = offset + this->subchunk1Size;

	this->audioFormat = FileUtil::pullShortData(fileData, offset);

	this->numChannels = FileUtil::pullShortData(fileData, offset);

	this->sampleRate = FileUtil::pullIntData(fileData, offset);

	this->byteRate = FileUtil::pullIntData(fileData, offset);

	this->blockAlign = FileUtil::pullShortData(fileData, offset);

	this->bitsPerSample = FileUtil::pullShortData(fileData, offset);

	// Sometimes there are extra params; if so, add to our offset the expected chunk difference
	offset = subchunk1End;

	int sc2id = FileUtil::pullIntData(fileData, offset);
	if (sc2id != WavFile::DATA_HEADER) {
		printf("Failed to load file; subchunk 2 does not have 'data' tag\n");
		return;
	}

	this->subchunk2Size = FileUtil::pullIntData(fileData, offset);
}

/**
 * If the data for this object is edited, this function must be called to update the header chunk sizes.
 */
void WavFile::rehash() {
	if (this->subchunk2Size != (int)this->data.size()) {
		this->subchunk2Size = (int)this->data.size();
		this->subchunk1Size = WavFile::DEFAULT_SC1SIZE;
		this->chunkSize = this->subchunk2Size + WavFile::DEFAULT_SC2DIFF;
	}
}

/**
 * Saves header data and sound data to a given file.
 *
 * @param f The file (output) stream to write data to.
 */
void WavFile::save(std::ofstream& f) {
	FileUtil::write(f, WavFile::RIFF_HEADER);
	FileUtil::write(f, this->chunkSize);
	FileUtil::write(f, WavFile::WAVE_HEADER);
	FileUtil::write(f, WavFile::FMT_HEADER);
	FileUtil::write(f, this->subchunk1Size);
	FileUtil::write(f, this->audioFormat);
	FileUtil::write(f, this->numChannels);
	FileUtil::write(f, this->sampleRate);
	FileUtil::write(f, this->byteRate);
	FileUtil::write(f, this->blockAlign);
	FileUtil::write(f, this->bitsPerSample);
	FileUtil::write(f, WavFile::DATA_HEADER);
	FileUtil::write(f, this->subchunk2Size);

	unsigned int dataLength = this->data.size();
	for (unsigned int u = 0; u < dataLength; u++) {
		FileUtil::write(f, data[u]);
	}
}
