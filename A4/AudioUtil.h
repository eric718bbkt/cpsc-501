#ifndef AUDIOUTIL_H
#define AUDIOUTIL_H

// Use FFT (faster) instead of baseline mode (slower)?
#define FFT

#include <vector>
#include <algorithm>
#include <iterator>
#include <float.h>
#include <cmath>

class AudioUtil {
	friend class RegressionTests;

public:
	static const int METHOD_BASELINE = 1;
	static const int METHOD_FFT = 2;

	static void convertDataToSignal(std::vector<short>&, int, std::vector<double>&);
	static void convertSignalToComplex(std::vector<double>&, std::vector<double>&);
	static void convertSignalToData(std::vector<double>&, int, std::vector<short>&, bool);
	static void convolve(const int method, std::vector<double>&, std::vector<double>&, std::vector<double>&);

private:
	static void complexConvolve(std::vector<double>&, std::vector<double>&, std::vector<double>&);
	static void convolveMethod1(std::vector<double>&, std::vector<double>&, std::vector<double>&);
	static void convolveMethod2(std::vector<double>&, std::vector<double>&, std::vector<double>&);
	static void fourOneFft(std::vector<double>&, bool);
	static double normalize(double, double, double, double, double);
	static void postProcessComplex(std::vector<double>&);
};

#endif
