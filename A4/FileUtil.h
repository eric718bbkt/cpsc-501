#ifndef FILEUTIL_H
#define FILEUTIL_H

#include <fstream>
#include <vector>
#include <sys/stat.h>
#include <climits>

class FileUtil {
public:
	static bool exists(char* filePath);
	static std::vector<char> read(std::ifstream&);
	static int pullIntData(std::vector<char>&, int& offset);
	static short pullShortData(std::vector<char>&, int& offset);
	static void write(std::ofstream&, char);
	static void write(std::ofstream&, int);
	static void write(std::ofstream&, short);
};

#endif
