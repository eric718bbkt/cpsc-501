#include "RegressionTests.h"

#define TPASS true
#define TFAIL false

#define assertEquals(a, b, m) if (a != b) { std::cout << "Equality assertion failed: '" << m << "'; " << a << " != " << b << "\n"; return TFAIL; }
#define assertFloatEquals(a, b, m) if (std::fabs(a - b) > std::numeric_limits<double>::epsilon()) { std::cout << "Equality assertion failed: '" << m << "'; " << a << " != " << b << "\n"; return TFAIL; }
#define assertVectorEquals(a, b, m) assertEquals(a.size(), b.size(), m); for (unsigned int i = 0; i < a.size(); i++) { assertEquals(a[i], b[i], m); }
#define assertFloatVectorEquals(a, b, m) assertEquals(a.size(), b.size(), m); for (unsigned int i = 0; i < a.size(); i++) { assertFloatEquals(a[i], b[i], m); }

bool RegressionTests::displayTest(char* name, bool result) {
	printf("%s: %s\n", name, result == TPASS ? "PASS" : "FAIL");

	return result;
}

void RegressionTests::run() {
	bool testsPassed = TPASS;

	testsPassed &= RegressionTests::displayTest((char*)"testComplexConvolve", testComplexConvolve());
	testsPassed &= RegressionTests::displayTest((char*)"testFour1", testFour1());

	if (testsPassed == TPASS) {
		printf("\nAll tests passed!\n\n");
	} else {
		printf("\nOne or more tests FAILED!\n\n");
	}
}

bool RegressionTests::testComplexConvolve() {
	std::vector<double> dry;
	dry.push_back(0.2);
	dry.push_back(0.2);
	dry.push_back(-0.2);
	dry.push_back(0.5);

	std::vector<double> ir;
	ir.push_back(0.0);
	ir.push_back(0.7);
	ir.push_back(0.4);
	ir.push_back(-0.3);

	std::vector<double> result((dry.size() + ir.size()) * 2);

	// Build an array of precomputed values
	std::vector<double> expectedResult;
	expectedResult.push_back(-0.14);
	expectedResult.push_back(0.14);
	expectedResult.push_back(0.07);
	expectedResult.push_back(0.26);

	AudioUtil::complexConvolve(dry, ir, result);
	result.resize(4); // Just take the non-zero bits

	assertFloatVectorEquals(result, expectedResult, "Comparing convolution and expected result");

	return TPASS;
}

bool RegressionTests::testFour1() {
	std::vector<double> signal;
	signal.push_back(0.2);
	signal.push_back(0.2);
	signal.push_back(-0.2);
	signal.push_back(0.5);

	// Build an array of precomputed values
	std::vector<double> expectedSignal;
	expectedSignal.push_back(0.0);
	expectedSignal.push_back(0.7);
	expectedSignal.push_back(0.4);
	expectedSignal.push_back(-0.3);

	// Build an array of precomputed values
	std::vector<double> invertedSignal;
	invertedSignal.push_back(0.4);
	invertedSignal.push_back(0.4);
	invertedSignal.push_back(-0.4);
	invertedSignal.push_back(1.0);

	AudioUtil::fourOneFft(signal, true);

	assertFloatVectorEquals(signal, expectedSignal, "Comparing transformed signal and expected signal");

	AudioUtil::fourOneFft(signal, false);

	assertFloatVectorEquals(signal, invertedSignal, "Comparing transformed signal and inverted signal");

	return TPASS;
}
